/**
 * Define a set of template paths to pre-load
 * Pre-loaded templates are compiled and cached for fast access when rendering
 * @return {Promise}
 */
 export const preloadHandlebarsTemplates = async function () {
  return loadTemplates([

    // Actor partials.
    'systems/everway/templates/actor/parts/actor-features.hbs',
    'systems/everway/templates/actor/parts/actor-magics.hbs',
    'systems/everway/templates/actor/parts/actor-powers.hbs',
  ]);
};
